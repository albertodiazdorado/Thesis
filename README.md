# Efficient Convex Quadratic Optimization Solver for Embedded MPC Applications
## Master's Thesis report

This Master's Thesis project was conducted by Alberto Díaz Dorado, at the Department of Automatic Control in KTH, under the tuition of Pr. Mikael Johansson and the supervision of Arda Aytekin and Martin Biel. The project spawned from September 2017 until March 2018, accounting for 30 ECTS of work. The final report, as it was submitted, is available under **main.pdf**.

## Abstract
Model predictive control (MPC) is an advanced control technique that equires solving an optimization problem at each sampling instant.
Several emerging applications require the use of short sampling times to cope with the fast dynamics of the underlying process.
In many cases, these applications also need to be implemented on embedded hardware with limited resources.
As a result, the use of model predictive controllers in these application domains remains challenging.

This work deals with the implementation of an interior point algorithm for use in embedded MPC applications.
We propose a modular software design that allows for high solver customization, while still producing compact and fast code.
Our interior point method includes an efficient implementation of a novel approach to constraint softening, which has only been tested in high-level languages before.
We show that a well conceived low-level implementation of integrated constraint softening adds no significant overhead to the solution time, and hence, constitutes an attractive alternative in embedded MPC solvers.
