\chapter{Polymorphism and Generic Programming}\label{appendixA}
This Appendix gives insight into the concepts of \emph{polymorphism} and \emph{generic programming}, which are neeeded for Chapter~\ref{ch5:implementation} if the reader is not familiar with them.
Although these are generic Computer Science principles, we will use the programming language \code{C++} to introduce them.
The reason is that \code{C++} is the language of choice for the implementation of our software.

\section{Polymorphism}
In Computer Science, we say that an object is polymorphic when it \emph{inherits} behaviour from other objects.
The object you inherit from is the \emph{Parent}, and the object that inherits is called \emph{Child}.

What is special about inheritance is that childs share the behaviour of all its parents (in case that multiple inheritance is allowed, you can inherit from various parents at the same time).
This is where the name polymorphism comes from, since childs behave as if they were several objects at once.
Exploiting this principle, parents can be used to customize the behaviour of their children.

We shall illustrate this with an example.
Assume that we have an object (a \code{class} in \code{C++}) that stores a vector and provides a function to evaluate the norm of the vector.
(Object functions are called methods in \code{C++}, and we shall use this name from now on.)
The minimal interface of such object is depicted in Code~\ref{code:vector1}.
The method \code{norm} return the norm of the vector stored in the class.

\begin{lstfloat}[H]
	\begin{lstlisting}
class Vector{
private:
  int n;            // Vector dimension
  double* vector;   // Array to store the vector

public:
  double norm();    // Function that evaluates the norm
  };
  \end{lstlisting}
	\caption{Vector class with a method for evaluating the norm.}
	\label{code:vector1}
\end{lstfloat}

However, we may want to give the user the possibility of choosing between different types of norms.
Depending on the application, the user may need to use the 1-norm or the 2-norm.
Assume also that they are mutually exclusive: if the 1-norm is used, there is no need for the 2-norm.

One approach would be to implement two different methods (\eg, \code{norm1} and \code{norm2}), and let the user choose the appropriate one.
Nevertheless, then the user must change the signature in every piece of code.
We may, instead, define a single method that takes a boolean argument: \code{0} for the 1-norm, \code{1} for the 2-norm (Code~\ref{code:vector2}).

\begin{lstfloat}[H]
	\begin{lstlisting}
class Vector{
private:
  int n;            // Vector dimension
  double* vector;   // Array to store the vector

public:
  double norm(bool x){
    if (x==false)
      return norm1();
    else
    return norm2();
  }
  double norm1();   // Returns the norm-1 of the vector
  double norm2();   // Returns the norm-2 of the vector
  };
  \end{lstlisting}
	\caption{Vector class with a method for evaluating two different norms.}
	\label{code:vector2}
\end{lstfloat}

This way, the user may define the type of norm he or she wants to use at the beginning of the code, and always use the same signature.
An example is provided in Code~\ref{code:vector3}.
Note that the user can easily change every instance of the method \code{norm} by changing the value of the constant variable \code{x} in the first line.

\begin{lstfloat}[H]
	\begin{lstlisting}
constexpr bool x = false;   // Every instance of "norm" returns
                            // the 1-norm

int main(){
  int n{3};               // Vector dimension
  double[3] v{1,2,3};     // Create the array [1, 2, 3]

  Vector MyObject(n,v);   // Initialize the object with
                          // dimension and data
  MyObject.norm(x);       // Returns the 1-norm of the vector

  return 0;
}
  \end{lstlisting}
	\caption{Main code for using the class Vector.}
	\label{code:vector3}
\end{lstfloat}

However, note that this design has two of the main drawbacks that we mentioned before.
In first place, it unnecesarily expands the code footprint, since the code for both the 1-norm and the 2-norm is available, even if only one of them is used.
In this small examples, this is not important, but it can rapidly scalate for more complex software.

In second place, there is one \code{if-else} comparison (Code~\ref{code:vector2}, line 8) that makes the code slower and harder to read, debug and maintain.

We can, instead, define the behaviour of the method \code{norm} using inheritance.
First, we create two parent objects: one handles the 1-norm, the other one handles the 2-norm.

\begin{lstfloat}[H]
	\begin{lstlisting}
class norm1{
public:
  static double do_work(int n, double* x){
    double r{0};
    for (int i = 0; i < n; i++)
      r += abs(x[i]);
    return r;
  }
};

class norm2{
public:
  static double do_work(int n, double* x){
    double r{0};
    for (int i = 0; i < n; i++)
      r += x[i]*x[i];
    return sqrt(r);
  }
};
  \end{lstlisting}
	\caption{Parent objects.}
	\label{code:vector4}
\end{lstfloat}

Then, we provide our \code{Vector} class with the desired behaviour, by inheriting from the appropriate parent.

\begin{lstfloat}[H]
	\begin{lstlisting}
class Vector : public norm1{  // Inherits from norm1
  private:
    int n;                    // Vector dimension
    double* vector;           // Array to store the vector
      
  public:
    double norm(bool x)
      norm1::do_work(n,vector);   // The parent implements the
                                  // desired behaviour
     };
  \end{lstlisting}
	\caption{Child object.}
	\label{code:vector5}
\end{lstfloat}

We say that we delegate to the parent class the execution of the method.
This design avoids code \code{if-else} branching, saves execution time and does not increase the memory footprint unnecessarily.
Then, the user can safely use the 1-norm in his or her main program:

\begin{lstfloat}[H]
	\begin{lstlisting}
int main(){
  int n{3};               
  double[3] v{1,2,3};     

  Vector MyObject(n,v);   // Initialize the object with
                          // dimension and data
  MyObject.norm();        // Returns the 1-norm of the vector

  return 0;
}
  \end{lstlisting}
	\caption{Main code for using the class Vector using the inheritance design.}
	\label{code:vector6}
\end{lstfloat}

Note that we do not have to specify what type of \code{norm} we want to use, since the parent class will handle it by itself.

The polymorphism principle does help us to avoid any code branching and reduce the memory footprint to a minimum.
However, notice that this approach has a huge disadvantage that makes it difficult to use.
To switch from one policy to another, we have to modify the inheritance chain.
Although this is doable in small problems, it becomes cumbersome with larger problems.
For instance, think about the algorithm presented in Chapter~\ref{ch5:implementation}, which has \code{6859} possible inheritance chains.

\section{Generic programming}

In Computer Science, the paradigm of generic programming allows us to circumvent this problem of hard-coding every inheritance chain that we want to use.
According to the generic programming principle, we do not have to create specialized code for each particular instance of a family of problems.

In \code{C++}, generic programming is called \emph{template programming}.
According to~\cite{2012-Lippman}: "\textit{A template is a blueprint or formula for creating a class or a function}".
As the defination says, a template is not a piece of software itself; but a set of instructions for creating it.
We will clarify this with an example.

Assume the following definition of a regular function for computing the square of an integer numer in \code{C++}:
%
\begin{lstfloat}[H]
	\begin{lstlisting}
int square(int x){
  return x*x;
}
  \end{lstlisting}
\end{lstfloat}
We can pass any object of type \code{int} to the function, but it will not accept other types such as \code{double} or \code{float}.
In other words: given the problem of calculating the square of a number, the previous functions solves just a particular instance (the one of integer input data).
Using template programming, we can give instructions to form the appropriate code:
%
\begin{lstfloat}[H]
	\begin{lstlisting}
template <class T>
T square(T x){
  return x*x;
}
  \end{lstlisting}
\end{lstfloat}
%
Here, \code{square} is a \emph{templated function}.
It is not a piece of code yet, since the type \code{T} is not known \textit{a priori}.
However, we can use the function in our code.
We may call \code{square} on any data type that supports the product operation ("\code{*}") returning an object of the same type.
This is the case of all basic \code{C++} types: \code{int}, \code{float}, \code{double}, \code{uint\_16t}, etc.

Whenever we compile the source code, the compiler will generate the necessary functions, and only the necessary functions.
For instance: if we call \code{square} on objects of type \code{double}, it will generate a regular \code{C++} function, that takes \code{double} as an argument and returns \code{double}.
As opposed to it, the compiler will generate no code for the square function for integers, since it is not used.

Interestingly, if the function is not used, the compiler will just ignore it.
In plain English: the compiler will work as if the templated function never was typed.

Template programming offers great potential when it comes to savings in the size of both the source code and the compiled code.
The combination of polymorphism (to particularize behaviour) and generic programming (to specify the policy that we want to use without modifying the source code) is called Policy-Based Design.