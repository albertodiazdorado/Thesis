\chapter{Numerical Experiments}
\label{ch6:num_exp}

In this chapter, we report some performance measurements of the designed software.
For benchmarking purposes we will compare against qpOASES~\cite{2014-Ferreau}, a parametric active set solver, and Gurobi, a general purpose comercial solver.
Our numerical experiments put emphasis in two main characteristics of optimization solvers: effectivity and efficiency.

We measure the effectivity of our software as the accuracy of the solutions it provides.
To do so, we compare the solution reported by our solver with the solution reported by Gurobi.
The reason for this is that Gurobi is solid, well-test commercial package.

Efficiency refers to the time that the software requires to solve a given optimization problem (to a given accuracy).
Here, the choice is qpOASES for several reasons.
First, it is an active set solver, and hence allows to compare against a completely different algorithmic approach.
Second, qpOASES is one of the best accepted open-source QP solvers in the optimization community.
It is common to see qpOASES in the benchmark of any new optimization software (see \eg the excellent benchmarking in~\cite{2016-Santin}).
And, finally, qpOASES is open source and hence we can easily make use of it.

\section{Benchmarking Problem: the Oscillating Masses}
Our focus in in the optimization problems that arise from an MPC structure.
Hence, it would have been possible to use randomly generated MPCs.
However, in our benchmarking we will apply our optimization solver to a real MPC problem.
The control problem that we are atttempting to solve is a mechanical system of masses interconnected by springs, as depicted in Figure~\ref{fig:osc_masses}.
This setup is a popular benchmarking problem for optimization in the scope of Model Predictive Control (see~\cite{2010-Wang, 2012-Domahidi, 2016-Santin}).


\begin{figure}
	\centering
	\begin{tikzpicture}[
		node distance = 0mm,
		start chain = going right,
		box/.style = {draw,
				font=\linespread{0.75}\selectfont\small,
				align=center, inner sep=2mm, outer sep=0pt,
				minimum width=1cm,minimum height=1cm,
				on chain},
		axs/.style = {draw, minimum width=12mm, minimum height=2mm,
				inner sep=0pt, outer sep=0pt,
				on chain, node contents={}},
		ground/.style = {fill,pattern=north east lines,
				draw=none,minimum width=0.75cm,
				minimum height=0.3cm},
		arr/.style = {color=#1, line width=0.8mm,
		shorten >=-1mm, shorten <=-1mm,
		-{Stealth[length=1.6mm,width=3mm,flex=1.2]},
		bend angle=60},
		spring/.style = {thick, decorate,       % new, 
				decoration={zigzag,pre length=1mm,post length=1mm,segment length=6}
			},
		shorten <>/.style = {shorten >=#1, shorten <=#1},
		]
		% blocks (boxes)
		\node (m1) [box] {$m_1$};
		\node (wall) at (m1.west) [ground, rotate=-90, minimum width=1.5cm,yshift=-1.2cm] {};
		\draw (wall.north east) -- (wall.north west);
		% \draw [spring] (wall.north east) -- (m1.west);
		\draw [spring] (wall.100) -- ($(m1.north west)!(wall.100)!(m1.south west)$);
		% \node at (-0.8,0.5) {$k_1$};
		\node (k1) at (m1.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_1$};
		\node (m2) at (m1.west)[box,xshift=1cm] {$m_2$};
		\node (m3) at (m2.west)[box,xshift=1cm] {$m_3$};
		\node (m4) at (m3.west)[box,xshift=1cm] {$m_4$};
		\node (wall2) at (m4.east) [ground, rotate=90, minimum width=1.5cm,yshift=-1.2cm] {};
		\draw (wall2.north east) -- (wall2.north west);
		\draw [spring] (wall2.100) -- ($(m4.north east)!(wall2.100)!(m4.south east)$);
		\draw [spring] (m1.east) -- (m2.west);
		\node (k2) at (m2.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_2$};
		\draw [spring] (m2.east) -- (m3.west);
		\node (k3) at (m3.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_3$};
		\draw [spring] (m3.east) -- (m4.west);
		\node (k4) at (m4.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_4$};
		\node (k5) at (m4.east) [font=\selectfont\footnotesize,xshift=0.5cm,yshift=0.4cm] {$k_5$};

		\node (u1) at (m2.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=1cm] {$u_1$};
		\node (u2) at (m3.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=-1cm] {$u_2$};
		\node (u3) at (m4.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=1cm] {$u_3$};

		\draw [->,to path={|- (\tikztotarget)}] (m1) edge (u1);
		\draw [->,to path={|- (\tikztotarget)}] (m2) edge (u1);
		\draw [->,to path={|- (\tikztotarget)}] (m2) edge (u2);
		\draw [->,to path={|- (\tikztotarget)}] (m3) edge (u2);
		\draw [->,to path={|- (\tikztotarget)}] (m3) edge (u3);
		\draw [->,to path={|- (\tikztotarget)}] (m4) edge (u3);
	\end{tikzpicture}
	\caption{Oscillating masses system}
	\label{fig:osc_masses}
\end{figure}

In the system, there are $N$ masses, interconnected by $N+1$ springs and connected to the walls at both ends of the chain.
In the resting position of the masses, the springs exert no forces upon any of them.
The position of the $i$-th mass is denoted by $x_i(t)$, where $x_i(t)=0$ indicates that the mass lays on its resting position.

The control inputs are external forces that act upon the masses, and we add them as an independent term to the dynamic equations of the masses upon which they act.
If we denote the inputs by $u$, the dynamic equations for the system depicted in Figure~\ref{fig:osc_masses} are:
%
\begin{align}
	m_1\ddot{x}_1 & = -k_1x_1 + k_2(x_2-x_1) +u_1 ,    \label{ch6:aux1} \\
	m_2\ddot{x}_2 & = -k_2(x_2-x_1) + k_3(x_3-x_2) -u_1+u_2,            \\
	m_3\ddot{x}_3 & = -k_3(x_3-x_2) + k_4(x_4-x_3)-u_2+u_3,             \\
	m_3\ddot{x}_3 & = -k_4(x_4-x_3) - k_5x_4 -u_3 \label{ch6:aux2},
\end{align}
%
where $m_i$ is the $i$-th mass, $k_i$ is the mechanical constant of spring $i$ and the dependency of $x_i$ with time has been omitted for clarity.
Note that the system can be easily expanded to any number of masses.

To use this model in our MPC simulations, we have to derive a state-space model and discretize it.
Details on this process are available in Appendix~\ref{appendixB}.
Here it suffices to mention that the resulting state-space model has as many inputs as actuators on the masses, and twice as many states as masses in the system (since both positions and velocities are incorporated to the state vector).

\section{Test Procedure}
In order to make the efficiency tests on an even basis, we enable the MPC option in qpOASES, which results in faster execution of the algorithms.
In Gurobi, we do not use any special option, since we are not interested in computation speed but in the accuracy of the solution.
We performed all the tests in C++.
The reason for this is that both qpOASES and our solver source code is written in C++.
In the case of Gurobi, we use the C++ wrapper provided with the package.

We have performed all the numerical experiments in a server with $1.2$ terabytes of RAM and $32$ Intel cores E5-2687W and  $3.10$ GHz.
We limited the computations to just one core.
We use a sampling time of $0.5$ seconds and the simulations last for $500$ time steps.
Additionally, we reproduced every simulation $50$ times, and the smallest solution time for every QP was recorded.
This way, we attempt to eliminate the influence of other programs running on the computer.
Finally, since we are interested in online MPC, where every optimization problem must be solved before the next sampling interval, the worst execution time for every simulation is reported.

In other words: every QP in the simulation is solved $50$ times, and the smallest solution time is recorded.
Then, among all the QPs in the simulation, the worst computation time is reported.
For completeness, we als report the median of the solution time, the number of required iterations and the accuracy of the solutions.

It is also important to mention that we include a random disturbance in our tests.
To make sure that we generate the same QPs in every simulation, we always use the same \emph{seed} for generating the random numbers.

\section{Test 1: MPC Dimensions Influence}
In this section, we study how the dimensions of the system under control (number of states $n$ and inputs $m$) and the prediction horizon $T$ influence the performance of the designed software.
The base case is depicted in Figure 3:

\begin{figure}
	\centering
	\begin{tikzpicture}[
		node distance = 0mm,
		start chain = going right,
		box/.style = {draw,
				font=\linespread{0.75}\selectfont\small,
				align=center, inner sep=2mm, outer sep=0pt,
				minimum width=1cm,minimum height=1cm,
				on chain},
		axs/.style = {draw, minimum width=12mm, minimum height=2mm,
				inner sep=0pt, outer sep=0pt,
				on chain, node contents={}},
		ground/.style = {fill,pattern=north east lines,
				draw=none,minimum width=0.75cm,
				minimum height=0.3cm},
		arr/.style = {color=#1, line width=0.8mm,
		shorten >=-1mm, shorten <=-1mm,
		-{Stealth[length=1.6mm,width=3mm,flex=1.2]},
		bend angle=60},
		spring/.style = {thick, decorate,       % new, 
				decoration={zigzag,pre length=1mm,post length=1mm,segment length=6}
			},
		shorten <>/.style = {shorten >=#1, shorten <=#1},
		]
		% blocks (boxes)
		\node (m1) [box] {$m_1$};
		\node (wall) at (m1.west) [ground, rotate=-90, minimum width=1.5cm,yshift=-1.2cm] {};
		\draw (wall.north east) -- (wall.north west);
		% \draw [spring] (wall.north east) -- (m1.west);
		\draw [spring] (wall.100) -- ($(m1.north west)!(wall.100)!(m1.south west)$);
		% \node at (-0.8,0.5) {$k_1$};
		\node (k1) at (m1.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_1$};
		\node (m2) at (m1.west)[box,xshift=0.9cm] {$m_2$};
		\node (m3) at (m2.west)[box,xshift=0.9cm] {$m_3$};
		\node (m4) at (m3.west)[box,xshift=0.9cm] {$m_4$};
		\node (m5) at (m4.west)[box,xshift=0.9cm] {$m_5$};
		\node (m6) at (m5.west)[box,xshift=0.9cm] {$m_6$};
		\node (wall2) at (m6.east) [ground, rotate=90, minimum width=1.5cm,yshift=-1.2cm] {};
		\draw (wall2.north east) -- (wall2.north west);
		\draw [spring] (wall2.100) -- ($(m6.north east)!(wall2.100)!(m6.south east)$);
		\draw [spring] (m1.east) -- (m2.west);
		\node (k2) at (m2.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_2$};
		\draw [spring] (m2.east) -- (m3.west);
		\node (k3) at (m3.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_3$};
		\draw [spring] (m3.east) -- (m4.west);
		\node (k4) at (m4.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_4$};
		\draw [spring] (m4.east) -- (m5.west);
		\node (k5) at (m5.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_5$};
		\draw [spring] (m5.east) -- (m6.west);
		\node (k6) at (m6.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=0.4cm] {$k_6$};
		\node (k7) at (m6.east) [font=\selectfont\footnotesize,xshift=0.5cm,yshift=0.4cm] {$k_7$};

		\node (u1) at (m2.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=1cm] {$u_1$};
		\node (u2) at (m3.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=-1cm] {$u_2$};
		\node (u3) at (m4.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=1cm] {$u_3$};
		\node (u4) at (m5.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=-1cm] {$u_4$};
		\node (u5) at (m6.west) [font=\selectfont\footnotesize,xshift=-0.5cm,yshift=1cm] {$u_5$};

		\draw [->,to path={|- (\tikztotarget)}] (m1) edge (u1);
		\draw [->,to path={|- (\tikztotarget)}] (m2) edge (u1);
		\draw [->,to path={|- (\tikztotarget)}] (m2) edge (u2);
		\draw [->,to path={|- (\tikztotarget)}] (m3) edge (u2);
		\draw [->,to path={|- (\tikztotarget)}] (m3) edge (u3);
		\draw [->,to path={|- (\tikztotarget)}] (m4) edge (u3);
		\draw [->,to path={|- (\tikztotarget)}] (m4) edge (u4);
		\draw [->,to path={|- (\tikztotarget)}] (m5) edge (u4);
		\draw [->,to path={|- (\tikztotarget)}] (m5) edge (u5);
		\draw [->,to path={|- (\tikztotarget)}] (m6) edge (u5);
	\end{tikzpicture}
	\caption{Base benchmarking problem}
	\label{fig:base_case}
\end{figure}

It is a system with $N = 6$ masses, $7$ springs and $5$ actuators, along with a prediction horizon of $20$ time steps.
This produces a state-space model with $n=12$ states and $m=5$ inputs.
All the masses in the system weigth $1 $kg, and all the spring constants are equal to $1$ kg/s.

In the initial state, all masses lay on their resting position with zero velocity.
The goal is to control the system to the resting position of all the masses when random disturbances come in.
To do so, the system is subject to some constraints on displacements, velocities and inputs.
In particular, the restrictions are:
%
\begin{align*}
	\left|x_i[k]\right| & \leq 1   &  & \forall i,k, \\
	\left|v_i[k]\right| & \leq 1   &  & \forall i,k, \\
	\left|u_i[k]\right| & \leq 0.5 &  & \forall i,k,
\end{align*}
%
where $\left| \cdot \right|$ is the absolute value operator.

The random disturbance $d \in \mathbb{R}^N$ acts upon every mass and has a uniform distribution $[-0.357, 0.357]$.
We chose those limits such that none of the arising optimization problems becomes infeasible.
Finally, the penalty matrices are chosen as $R=I$ and $Q=P=I$, where $I$ is the identity matrix of appropriate dimensions.

\subsection*{Dimensions of System under Control}
First, we will show how the system dimensions influence the performance of our software.
To do so, we will solve the base case with a varying number of masses, ranging from $N=4$ to $N=20$.
One actuator is added for every additional mass, such that there are always $m=N-1$ inputs to the system (which guarantees controllability under the given disturbances).
The results are depicted in Figure~\ref{fig:masses}.

\begin{figure}
	\input{Figures/Experiment1/nmax.tex}
	\caption{Maximum and mean computation time for varying number of masses}
	\label{fig:masses}
\end{figure}

\begin{figure}[h]
	\input{Figures/Experiment1/nmax-2.tex}
	\caption{Maximum and mean iterations for varying number of masses}
	\label{fig:masses_iters}
\end{figure}

We would like to make a few remarks here.

In first place, we shall notice that the designed software and qpOASES are comparable regarding maximum compuation time, for all problem dimensions.
The largest QP problem in the series has $20$ masses and $19$ actuators.
When spawned along $T=20$, it results in a QP with $800$ variables and $380$ inputs, all of which are lower and upper bounded.
Even in that case, the computation time lies below $100$ ms, which is significantly smaller than the sampling time of $500$ ms.

In second place, qpOASES shows a clear advantage regarding the mean computation time.
This is easy to understand by looking at Figure~\ref{fig:masses_iters}.
qpOASES is an active set method and, hence, requires very few iterations when a good initial guess is available.
Since we are simulating a linear system with a rather small disturbance, this is usually the case.
qpOASES reports an average number of iterations below $1$ for most of the problems.
Our interpretation is that, for the majority of the QPs in the simulation, the set of active constraints at the optimum does not change.
Under such circumstances, any active set method does not require any iteration, since it converges to the optimum in exactly one Newton step.
Hence, it is natural to expect a low mean computation time.

On the other hand, the iterations required by Mehrotra are rather constant for all problem dimensions.
This is advantageous regarding the maximum number of iterations.
There is an increased optimization times for large problems, but we explain it in terms of the computational effort required for the factorization of large matrices, as opposed to requiring more steps to converge.
That is the case of qpOASES.
However, the minimum iterations required by our software is rarely below $3$.
One possible explanation is that interior point methods need to reach the central path before finding a new optimal value.
Assume that the solutions of two consecutive QPs lie in the same set of active constraints, which is often the case.
Whereas qpOASES converges in one Newton step, our solver needs to step aside the boundary, reach the central path and converge to the new optimum.
The warmstart of interior point methods is still an open question, with few satisfactory approaches in the literature.

To sum up, it is important to remark that, for our purposes, the maximum computation time is what matters.
Out goal is to solve MPC problems in real time and, hence, we need to solve the given optimization programs in a fixed, maximum time.
However, it is worth to mention that a low mean computation time is probably advantageous in other aplications where the total computation time matters (\eg in simulations).


\subsection*{Dimensions of the Input Vector}
Next, we show how the ratio of system inputs to system states affects the performance of our solver.
For that purpose, we fix the number of masses to six, and the prediction horizon length to twenty time steps.

In addition, we create MPC problems with a varying number of actuators, ranging from $m=3$ to $m=11$ (\ie the input space is always smaller than the state space).
The first five actuators are the same as in the previous experiment.
To reduce the number of inputs, we remove the left-most actuators; \ie the actuator between masses $1$ and $2$, first, and the one conected to masses $2$ and $3$, later.
To increase the number of inputs, we add small actuators that act on a single mass.

These actuators are constrained to a $20$\% of the main actuators; \ie they are upper and lower bounded by $0.1$ kg.m/s\textsuperscript{2}.
The first additional input acts upon mass $6$, the second one acts upon mass $5$, and so on.

\begin{figure}[h]
	\input{Figures/Experiment1/mmax-2.tex}
	\caption{Maximum and mean iterations for varying number of inputs}
	\label{fig:inputs_iters}
\end{figure}

\begin{figure}
	\input{Figures/Experiment1/mmax.tex}
	\caption{Maximum and mean computation time for varying number of inputs}
	\label{fig:inputs}
\end{figure}

These experiments are interesting because the show the consequences of choosing a dense of sparse MPC formulation.
Recall from Chapter~\ref{ch3:stateoftheart} that the dense MPC formulation results in a QP with $Tn$ variables and no equality constraints.
In contrast, the sparse MPC results in a QP with $T(m+n)$ variables and additional equality constraints.

In theory, interior points methods tend to perform a small, rather constant number of expensive iterations when compared to active set methods.
This agrees with the results we report in Figure~\ref{fig:masses} and Figure~\ref{fig:masses_iters}: whereas qpOASES performs a much larger maximum number of iterations, the maximum computation time is similar for qpOASES and Mehrotra.

In Figure~\ref{fig:inputs_iters} we observe something different.
The maximum number of iterations is approximately equal for out interior point implementation and qpOASES.
In theory, this should result in a much better performance of the active set method, since its iterations tend to be cheaper.
However, Figure~\ref{fig:inputs} shows that the maximum computation time is also approximately equal.

The reason for this is that any algorithm that solve the MPC in dense formulation is specially sensitive to the input space dimension.
A sparse MPC formulation is less sensitive to this parameter, as can be seen in Figure~\ref{fig:inputs}.

\subsection*{Prediction Horizon Length}
Finally, we will show how our software behaves with regard to the prediction horizon length.
Note that a larger number of prediction steps may be due to a larger prediction time, but also due to a smaller sampling time.
In our experiments, we fix the number of masses to six and the number of actuators to five (\ie $N=12$ states and $m=5$ inputs).
Then, we reproduce the simulations for prediction horizons that range from 10 to 100 steps.
%
\begin{figure}
	\input{Figures/Experiment1/tmax.tex}
	\caption{Maximum and mean computation time for varying prediction steps}
	\label{fig:horizon}
\end{figure}
%
\begin{figure}[h]
	\input{Figures/Experiment1/tmax-2.tex}
	\caption{Maximum and mean iterations for varying prediction steps}
	\label{fig:horizon_iters}
\end{figure}

The results depicted in Figure~\ref{fig:horizon} agree with the theory.
Whereas qpOASES has a better maximum computation time for smaller prediction horizons, our solver performs notably well under long prediction horizons.
The maximum computation time for Mehrotra grows linearly with the prediction horizon, which is in accordance with the complexity analysis in Chapter~\ref{ch4:approach}.
(Note that a linear function has the typical form of a logarithmic function when plotted against a logarithmic axis.)

Even more interesting, the maximum and mean number of iterations required by our solver is constant for all prediction horizons (Figure~\ref{fig:horizon_iters}).
This is one big advantage of interior point methods, at the cost of a high minimum number of iterations required.

\section{Test 2: Accuracy of the Solution}
The goal of this work is to solve the quadratic programs efficiently, but not at the expense of obtaining a correct solution.
From our simulations, we can conclude that our software provides rather accurate optimal values, since the system is controlled succesfully.
However, quantifying the accuracy of the solution is not a simple task.
Furthermore, we noticed in the course of our experiments that there was some discrepancy between the solutions provided by Mehrotra and qpOASES to a series of critical QPs.

We use a third optimization software, Gurobi, to check the correctness of the optimal values and variables.
To do so, we reproduced the same batch of experiments from Test 1, using the same seeds for the random number generator such that the results are comparable.
Also, we assume in this section that Gurobi provides the correct solution, and thus we compare against it.

In our software, we set both the \emph{feasibility tolerance} and the \emph{optimality tolerance} to $1e-3$, for all performance and accuracy tests.
Recall that the feasibility tolerance is the minimum treshold for the norm of the equality residual~\eqref{ch4:KKT}.
That is: we assume that the solution is feasible with respect to the equality constraints if the norm of $r_e$ is smaller thatn the feasibility tolerance.
Similarly, the optimality tolerance is a minimum treshold for the duality gap.

In our accuracy tests, we vary the number of states, inputs and prediction steps as we did in the previous section.
In Figure~\ref{fig:state-accuracy} we show the maximum offset between the optimal value (provided by Gurobi) and the solutions of Mehrotra and qpOASES for a varying number of masses.
To do so, we have simulated the system for $500$ steps and report here the maximum and the mean offsets (in percentage).
From now on, \emph{offset} refers to the quantity $f(x)-f(x^*)$, where $f(x^*)$ is the optimal value as reported by Gurobi and $f(x)$ is the optimal value of the solver under consideration (Mehrotra, qpOASES).

\begin{figure}
	\centering
	\input{Figures/Experiment2-Accuracy/state-accuracy.tex}
	\caption{Maximum and mean error in the optimal value when compared against Gurobi.}
	\label{fig:state-accuracy}
\end{figure}

\begin{figure}[h]
	\centering
	\input{Figures/Experiment2-Accuracy/state-accuracy-bis.tex}
	\caption{Maximum and mean error in the optimal value when compared against Gurobi. (Log version)}
\end{figure}

First of all, we want to remark the accuracy of the solutions computed by Mehrotra.
Though the blue line in the plot is hard to read, the maximum error incurred in by Mehrotra lies below $0.01$\%, which is satisfactory.

Second, it is important to remark that qpOASES also does a good job when it comes to the correctness of the solution.
The mean error, when compared with Gurobi, lies below $0.06$\% in our numerical experiments.
However, we can observe that, for some critical QPs in the simulations, the error commited by qpOASES reaches values above $100$\%.

In fact, qpOASES provides reports whether a particular QP has been solved or not.
Based on that information, we have elaborated Table~\ref{table:n_unsolved}.
We report there how often qpOASES failed to solve a QPs in the simulation series.
%
\begin{table}
	\begin{center}
		\begin{tabular}{c|c}
			$N$  & \% unsolved QPs \\
			\hline
			$4$  & $0$             \\
			$5$  & $0$             \\
			$6$  & $0$             \\
			$7$  & $0$             \\
			$8$  & $0$             \\
			$9$  & $0$             \\
			$10$ & $2.2$           \\
			$11$ & $1.8$           \\
			$12$ & $1.8$           \\
			$13$ & $1.8$           \\
			$14$ & $2.0$           \\
			$15$ & $3.2$           \\
			$16$ & $4.6$           \\
			$17$ & $3.4$           \\
			$18$ & $6.2$           \\
			$19$ & $6.4$           \\
			$20$ & $6.6$
		\end{tabular}
	\end{center}
	\label{table:n_unsolved}
	\caption{Percentage of unsolved QPs in the simulations corresponding to different number of masses.}
\end{table}
%
These data are important because, as we can read from the qpOASES reference manual, enabling the MPC flag sacrifices accuracy and robustness in exchange for performance.
Hence, it is even more remarkable that Mehrotra provides a fast solution to the arising QPs, yet does not give up on accuracy on exchange.

To clarify the meaning of these numbers, we also provide the evolution of the optimal values for all the QPs in the problem scenario with $20$ masses.
For most of the time, the optimal value reported by the three solvers is indistinguishable from one another.
Nonetheless, for certain, critical QPs, qpOASES clearly fails at computing the optimal value.
Though we did not deepen in this analysis, this may suggest that employing qpOASES as an optimization backend could lead this particular system to instability.

\begin{figure}
	\centering
	\input{Figures/Experiment2-Accuracy/optval_evolution.tex}
	\caption{Optimal value for each simulation step, as reported by the Mehrotra, qpOASES and Gurobi.}
\end{figure}

We carried out the same correctness verification for the other two batches of numerical experiments: varying number of inputs, and varying number of the prediction horizon length.
Since the results are less relevant, we will comment them briefly.

\begin{figure}
	\centering
	\input{Figures/Experiment2-Accuracy/input-accuracy.tex}
	\caption{Maximum and mean error in the optimal value when compared against Gurobi. Varying number of inputs.}
	\label{fig:input-accuracy}
\end{figure}

In Figure~\ref{fig:input-accuracy} we can observe that both Mehrotra and qpOASES provide high quality solutions to all the QPs in the series of varying number of inputs.
In particular, the maximum error when compared against Gurobi lies always below $0.5$\%.
Notice that, though qpOASES proves to be more accurate than our software in this particular batch of problems, the difference is way less significant than in the previous series.

\begin{figure}
	\centering
	\input{Figures/Experiment2-Accuracy/horizon-accuracy.tex}
	\caption{Maximum and mean error in the optimal value when compared against Gurobi. Varying number of prediction steps.}
	\label{fig:horizon-accuracy}
\end{figure}

Finally, we report the correctness of our solver for problems of different prediction steps.
Figure~\ref{fig:horizon-accuracy} shows a similar error treshold to the previous one (Figure~\ref{fig:input-accuracy}).
Hence, we can conclude that our software is capable of performing efficient optimization procedures while keeping a high degree of accuracy.
While qpOASES can certainly outperform our solver in some cases (mainly during steady-state operation), it does not guarantee the correctness of the solution to the same extent that we do.
Finally, it is worth to remark that qpOASES did solve all the QPs in the two last problem batches.

\section{Test 3: Constraint Softening Implementation}
In this test, we report the impact that constraint softening has in the performance of out method.
We implement soft constraints as we explained in Chapter~\ref{ch4:approach}.

Here, we solve the exact same batch of problems that we solved in Test 1, but we let the constraints be soft.
Naturally, the soft constraints are never violated, since otherwise some of the QPs in Test 1 would have been infeasible.
For a comparison of our soft constraints method versus the standard method, we refer the reader to Test 4 and Test 5.

\begin{figure}
	\centering
	\input{Figures/Experiment3-SoftvsHard/softnmax.tex}
	\caption{Maximum and mean computation time when solving the problem with hard and soft constraints, for different number of masses.}
	\label{fig:softnmax}
\end{figure}

\begin{figure}
	\centering
	\input{Figures/Experiment3-SoftvsHard/softmmax.tex}
	\caption{Maximum and mean computation time when solving the problem with hard and soft constraints, for different number of inputs.}
	\label{fig:softmmax}
\end{figure}

\begin{figure}
	\centering
	\input{Figures/Experiment3-SoftvsHard/softtmax.tex}
	\caption{Maximum and mean computation time when solving the problem with hard and soft constraints, for different number of prediction steps.}
	\label{fig:softtmax}
\end{figure}

Figures~\ref{fig:softnmax} and~\ref{fig:softtmax} are interesting, since they show an \textit{a priori} counter-intuitive result.
The exact same problem is solved faster if soft constraints are enabled.
The difference is not great, but certainly the addition of soft constraints does not degradate the performace.

We have a possible explanation for this behaviour.
Whether we use hard or soft constraints, the search direction is computed at the exact same cost.
Moreover, during the simulations we rarely hit the state constraints, whereas we hit the input constraints quite often.
Hence, the linesearch also yields the same results.

Furthermore, we save one linesearch, since we do not take into account any hard constraints.
Furthermore, we do not have to calculate slack variables and lagrange multipliers associated with the hard constraints.
These are minor aspects of the algorithm, but they do contribute to explain this unexpected result.

Another question that we should ask ourselves is:
if soft constraints reduce the computational burden, shall we just soften all the constraints?
There are two main disadvantages in that approach.

First, it may be, depending on the penalties, that the optimal solution consists in violating a soft constraints, even if a feasible solution is available.
Then, the feasible solution is suboptimal and we disregard it.
Whereas this is acceptable for state constraints, it would be very dangerous in the case of input constraints.
(Recall that state constraints are normally control goals, but input constraints relate to physical limitations of the system.)

Second, we lose information about the problem whenever we substitute hard constraints by soft constraints.
For instance: if all constraints were soft, we could not perform exact linesearch.
Instead, we should rely on some backtracking linesearch~\cite{2010-Wang} and the application of the Mehrotra algorithm is unclear.
We did not investigate this direction, but we shall remark that choosing full constraint softening is not as advisable as Figures~\ref{fig:softnmax} and~\ref{fig:softtmax} may suggest.

Finally, we would like to pull the attention to Figure~\ref{fig:softmmax}.
Notice that we included an additional simulation with respect to Figure~\ref{fig:inputs}, the one corresponding to $m=2$.
We did so to illustrate the meaning of constraint softening.
In the simulation with $N=6$ masses and $m=2$ inputs, the optimization problem becomes infeasible at some simulation step.
In that case, the hard-constrained solver fails to provide a solution and iterates until reaching the maximum iterate count.

For the rest of the proposed systems (\ie, for $m$ ranging from $3$ to $11$), the results are in accordance with the previous discussion.

\section{Test 4: Constraint Softening Performance}
Next, we raise the disturbance to the interval $[-1,1]$, which guarantees that some of the QPs in the series are infeasible.
For this setup, we include qpOASES in the analysis.

We want to warn the reader against the meaning of the following results.
We will observe in the graphs that qpOASES is up to two orders of magnitude slower than our software.
However, this is not really the case.
It is the fact that qpOASES fails to converge in some of these problems (which are feasible by definition).
In those cases, it iterates until reaching the maximum iteration count, which we set to $1000$, and hence the solve times that are meaningless.

\begin{figure}
	\centering
	\input{Figures/Experiment4-Soft/nmax.tex}
	\caption{Maximum and mean computation time for soft constrained problems, for a varying number of masses.}
	\label{fig:soft1}
\end{figure}

\begin{figure}[H]
	\centering
	\input{Figures/Experiment4-Soft/nmax-2.tex}
	\caption{Maximum and mean number of iterations for soft constrained problems, for a varying number of masses.}
	\label{fig:soft2}
\end{figure}

Consider the performance of the solver for a varying number of masses, which is depiceted in Figures~\ref{fig:soft1} and~\ref{fig:soft2}.
First, we can observe the peaks that we just mentioned:
in some series, qpOASES encounters a QP problem that it cannot solve (\ie, the series for $N=6$, $12$, $15$ and $19$).
In those cases, the iteration count raises up to $1000$ (Figure~\ref{fig:soft2}), which is reflected in the maximum computation time (Figure~\ref{fig:soft1}).

Instead, we would like to focus on the other cases.
Notably, our software outperforms qpOASES by one order of magnitude regarding maximum computation time.
Similarly, the maximum and mean number of iteration rapidly grows with respect to the hard constrained experiment.
All these results speak in favour of our software.

\begin{figure}
	\centering
	\input{Figures/Experiment4-Soft/mmax.tex}
	\caption{Maximum and mean computation time for soft constrained problems, for a varying number of inputs.}
	\label{fig:soft3}
\end{figure}

\begin{figure}[H]
	\centering
	\input{Figures/Experiment4-Soft/mmax-2.tex}
	\caption{Maximum and mean number of iterations for soft constrained problems, for a varying number of inputs.}
	\label{fig:soft4}
\end{figure}

The results for different number of inputs and prediction steps are also reported for completeness.
However, the previous analysis still holds without any modification.

Interestingly, qpOASES systematically fails for all prediction horizons larger than $T=20$.
That is: from the $500$ QPs solved during the simulation, there is always at least one that qpOASES cannot solve within the maximum number of iterations.

\begin{figure}
	\centering
	\input{Figures/Experiment4-Soft/tmax.tex}
	\caption{Maximum and mean computation time for soft constrained problems, for a varying number of prediction steps.}
	\label{fig:soft5}
\end{figure}

\begin{figure}[H]
	\centering
	\input{Figures/Experiment4-Soft/tmax-2.tex}
	\caption{Maximum and mean number of iterations for soft constrained problems, for a varying number of prediction steps.}
	\label{fig:soft6}
\end{figure}

\section{Test 5: Accuracy of Constraint Softening}
Finally, one open question remains.
We know that qpOASES fails at solving some of the problems with softened constraints.
However, is our software solving them correctly?
To answer this question, we compare against Gurobi as we did in Test 2.

The setup is as follows.
We solve the same batch of problems that we solved in Test 4.
Instead of running every simulation $50$ times, we solve them only once and we do not measure times, since we are not interested in performance.
We feed the exact same dense QP to qpOASES and Gurobi.
For Mehrotra, we use or particular constraint softening implementation.

\begin{figure}[ht]
	\centering
	\input{Figures/Experiment5-SoftAccuracy/state-accuracy.tex}
	\caption{Accuracy of the solutions for the soft-constrained problem, for varying number of masses.}
	\label{fig:softaccuracy1}
\end{figure}

In Figures~\ref{fig:softaccuracy1}, ~\ref{fig:softaccuracy2} and~\ref{fig:softaccuracy3}, we show that maximum error of Mehrotra and qpOASES at computing the optimal value, when compared against Gurobi.
We can observe that our solver always provides rather accurate solutions, with the maximum error always below $1$\%.
In contrast, qpOASES exhibits notable accuracy errors.

Note that even the mean error is large, going above $10$\% for $N>12$.
This time, the maximum errors reaches values has high as $200$\%, which is probably inacceptable for most applications.
There is one outsider in Figure~\ref{fig:softaccuracy2}, the one corresponding to $6$ masses and $4$ inputs.
The worst case for qpOASES in that scenario is an optimal value which is $100$ times larger than the optimal one.
The mean error goes up to $140$\% in that simulation, which means that qpOASES systematically fails at solving the optimization problems that we randomly generate.

\begin{figure}[ht]
	\centering
	\input{Figures/Experiment5-SoftAccuracy/input-accuracy.tex}
	\caption{Accuracy of the solutions for the soft-constrained problem, for varying number of inputs.}
	\label{fig:softaccuracy2}
\end{figure}

\begin{figure}[ht]
	\centering
	\input{Figures/Experiment5-SoftAccuracy/horizon-accuracy.tex}
	\caption{Accuracy of the solutions for the soft-constrained problem, for varying number of prediction steps.}
	\label{fig:softaccuracy3}
\end{figure}

The results follow the same trend if we look at Figure~\ref{fig:softaccuracy3}, where we iterate over the number of prediction steps.
Important here is that our software always provides an accurate solution, while keeping it up with qpOASES in terms of efficiency.

For better understanding of the numbers we reported here, we will show how the optimal value varies along some simulations.
First, we show in Figure~\ref{fig:evolutionpn5} a simulation where qpOASES is not that off the correct value.
Still, we can observe some critical steps in the gap 350-400 where the performance of the controller may be degradated.

\begin{figure}
	\centering
	\input{Figures/Experiment5-SoftAccuracy/optval_evolution.tex}
	\caption{Optimal value for each iteration, as reported by each solver. Problem with $5$ masses, $4$ inputs and $20$ prediction steps.}
	\label{fig:evolutionpn5}
\end{figure}

In Figure~\ref{fig:evolutionpn12}, we show the simulation for $12$ masses.
This simulation has a maximum error of $279$\% for qpOASES ($0.4$\% for Mehrotra), and a mean error of $14$\% for qpOASES ($0.02$\% for Mehrotra).
Even though qpOASES shows a good accuracy for most simulation steps, in some critical ones it completely fails at identifying the optimum.

\begin{figure}
	\centering
	\input{Figures/Experiment5-SoftAccuracy/optval_evolution2.tex}
	\caption{Optimal value for each iteration, as reported by each solver. Problem with $12$ masses, $11$ inputs and $20$ prediction steps.}
	\label{fig:evolutionpn12}
\end{figure}

Finally, we show optimal value for the QPs in the simulation with $N=6$ and $m=4$.
In this case, qpOASES would just not suffice as an optimization engine for our MPC controller.

\begin{figure}
	\centering
	\input{Figures/Experiment5-SoftAccuracy/optval_evolution3.tex}
	\caption{Optimal value for each iteration, as reported by each solver. Problem with $6$ masses, $4$ inputs and $20$ prediction steps.}
	\label{fig:evolutionpm4}
\end{figure}
