\chapter{Introduction}
\label{chp:intro}

This work addresses the implementation of an interior point algorithm for the solution of multi-stage quadratic programming (QP) problems.
Of particular interest are the QPs that arise in the context of model predictive control (MPC) applications.
We are interested in algorithms tailored to the special structure of this particular family of problems; hence, we do not intend to target general QPs.
We focus on linear, time-invariant (LTI) system models with convex quadratic penalty terms in MPC applications.
These applications result in convex QPs.
% However, thanks to the coupling of different
% variables in the problem via the LTI system models, we can exploit the MPC problem structure
% to come up with efficient implementations of solvers as opposed to general QP problems.

In the thesis, we first provide the necessary background on MPC and quadratic optimization.
Later, we review the existing algorithms in the literature.
Finally, we present the architecture for the implementation, which aims at tackling a variety of MPC problems in a flexible way without sacrificing much from efficiency.

In the subsequent sections, we try to motivate the need for a flexible and efficient solver by giving an MPC example.
Then, we list the goals and the outcome of the work, and finally give a brief outline of the contents of the thesis.

\section{Motivation}

Model predictive control is an advanced control technique that naturally handles multi-variable, multi-objective control problems.
It consists of four major components: \emph{a value function}, \emph{an internal
	system model}, \emph{a horizon length} and \emph{a constraint set}. The value
function encodes the penalties on deviations of the inputs and the system's
states from their desired values. At each sampling instant, MPC uses its internal
system model and the sampled state to predict the behaviour of the system's
future states in the coming fixed-length horizon. Using these predictions, MPC
minimizes its value function with respect to the possible input values, while
satisfying the constraints defined by its constraint set, and then applies the
optimal input to the system.

There are few characteristics that make MPC a superior alternative to traditional controllers, \eg, PID-type controllers.
First, MPC naturally handles MIMO (multiple-input, multiple-output) plants, whereas PID controllers are best suited for SISO (single-input, single-output) plants.
Second, MPC readily incorporates system and design constraints to the problem formulation.
% This allows to safely operate the system close to the constraints, which is potentially the most efficient operation point in many applications.
This allows for safely exploiting all the slack provided by the constraints, rather than sticking to a conservative operation area, which translates into a more efficient control of the system.
Last, MPC provides the optimal input for a given choice of control goals and associated weights.
In contrast, PID controllers are always suboptimal for any given value function.

The main drawback of MPC, which has prevented it from seeing wide use accross industries, is that it involves solving an optimization problem at each sampling instant.
Optimization techniques are computationally expensive and time consuming.
Furthermore, optimization problems may be infeasible or unbounded, in which case the solvers may fail to provide a feasible input to the system.
Because of these reasons, MPC has been traditionally used in the chemical and process industry, where the system dynamics are inherently slow and sampling times range from few minutes to hours.
This enables the solver to provide an accurate solution to the optimization problem, or even reformulate the problem if it is found infeasible.

Thanks to the improvements in optimization algorithms as well as the superior
capacity of computers and microprocessors, MPC has been employed in diverse
domains spanning marine applications~\cite{2011-Gutvik, 2014-Bo, 2014-Binder},
aircraft control~\cite{2013-Kufoalor} and power electronic devices~\cite{2014-Vazquez}.
These applications have different requirements on the sampling intervals. For
instance, while the marine applications require a sampling time in the order of
seconds, aircraft control and power electronics devices demand sampling times
in the order of milliseconds.

In spite of this progress, the solution of the optimization problem is still the bottleneck that prevents a more widespread utilization of MPC techniques.
Most automatic controllers are not executed in computers, which have a superior computational power, but in embedded devices, such as PLCs, micro-controllers, FPGAs, and ASICs~\cite{2017-Johansen}.
% here, be careful with ASICs and FPGAs. FPGAs are _way_ faster than any given
% computer. ASICs are used to do bitcoin mining, which you cannot do with the
% most sophisticated GPUs :)
These devices have limited memory and limited computation capability, which challenges the task of solving the optimization problem in time.
For this reason, developing efficient and robust quadratic programming (QP) solvers is they key to the widespread utilization of MPC controllers.

While general purpose solvers may be used to solve optimization problems arising from MPC, these problems have a special structure that can be exploited for better efficiency.
In other words, we can take advantage of the knowledge about the structure of the problem to reduce the computational effort of the algorithms.
% Are we reducing the computational complexity of the algorithms, as in O(T^3) -> O(T^2) ?
% Here, you have repetition. Above, in Introduction, we already talked about exploiting
% the structure. Maybe we should drop that and keep this part?
This is an active area of research. There have been proposals to adapt traditional techniques for solving QP problems to more specialized MPC-type problems.
This includes, among others, explicit MPC, first-order methods, active-set methods and interior-point methods.

Even though they share a uniform structure, different MPC formulations may lead to different optimization problems.
This work focuses on the so-called Linear MPC formulations, which involve convex quadratic value functions, linear constraints and LTI internal system models.

There are two main reasons for this choice.
In the first place, this type of MPC formulation covers a great variety of real-world problems, since detailed nonlinear models are not always available and linearizing the dynamics around a working point is a widespread technique in the control community.
In the second place, one popular approach to solve nonlinear MPC problems is to solve a sequence of convex quadratic optimization problems.
This is the case of sequential quadratic programming (SQP)~\cite[Chapter 18]{2000-Nocedal}, but also of some nonlinear interior point methods such as LANCELOT~\cite{2010-Conn}.

Within the family of Linear MPC, there are many variations and simplifications that make the arising optimization problem easier to solve.
These simplifications are not fanciful, but rather obey common and sensible MPC formulations.
For instance, the most common penalty is the Euclidean norm of states and inputs.
While it is also possible to put linear penalties on cross-products of states and inputs, this is rather unusual.
In addition, physical input constraints are usually just upper and lower bounded.
A more general formulation allows for constraints involving linear combinations of inputs and states but this is also a less common scenario.
Taking advantage of these particularities, rather than treating all QP problems in a generalized way, allows for better efficiency.

In this work, the proposed solver benefits from a flexible design that takes advantage of all possible nuances in the MPC formulation, while also solving the most general case.
The design is highly modular a well suited for future expansions.

\section{Goals of the Thesis}
The goal is to write an efficient QP solver for Linear MPC applications, that exploits the structure of the problem as much as possible.
In particular, the solver shall be:
\begin{enumerate}
	\item Effective: it shall provide the correct solution to the target optimization problems.
	\item Robust:
	      it shall be able to handle infeasible initial points, provide a feasible solution in case of early termination and, ideally, provide some support in case of infeasibility or unboundeness.
	      If the solver cannot handle any kind of degeneracies, they must be specified.
	\item Flexible: it shall cover as many different MPC formulations as possible.
	      The solver shall also allow for some parameters tuning such as, for instance, trade-off between constraint relaxation and optimality tolerances.
	\item Efficient: it shall provide a solution with respect to the given tolerance as fast as possible.
	      To do so, we shall take full advantage of the MPC formulation in order to
	      \begin{enumerate}
		      \item reduce the number of iterations, and,
		      \item reduce the cost of each individual iteration.
	      \end{enumerate}
\end{enumerate}

\section{The Target Optimization Problem and Limitations}
The details of the MPC formulation and the arising QP will be discussed in detail in the later chapters.
By now, it suffices to know that the final solver shall be able to handle MPC problems in the form:

\begin{align}\label{ch1:problem_statement}
	\begin{aligned}
		\minimize_{U,X} &  &  & \omit \rlap{$\sum_{k=0}^{T-1} \left\{ u_k^\top Ru_k + x_k^\top Qx_k \right\} + x_T^\top Px_T$}                  \\
		\st             &  &  & x_{k+1} = Ax_k + Bu_k,                                                                         & k=0,\ldots,T-1 \\
		                &  &  & l_u\leq u_k \leq u_u,                                                                          & k=0,\ldots,T-1 \\
		                &  &  & F_uu_k\leq f_u,                                                                                & k=0,\ldots,T-1 \\
		                &  &  & l_x\leq x_k \leq u_x,                                                                          & k=0,\ldots,T-1 \\
		                &  &  & F_xx_k\leq f_x,                                                                                & k=0,\ldots,T-1 \\
		                &  &  & l_t\leq x_T \leq u_t,                                                                                           \\
		                &  &  & F_tx_k\leq f_t
	\end{aligned}
\end{align}
%
In other words, it should support handling input, state, and terminal constraints (both hard and soft), and shall distinguish between box and linear constraints.
In addition, we will show how output constraints, output penalties, tracking problems and input-rate penalties can be included in the given formulation.

We would also like to state the implicit limitations of formulation~\eqref{ch1:problem_statement}. It
\begin{enumerate}
	\item only allows for convex quadratic optimization problem definition,
	\item does not provide a wrapper for nonlinear MPCs,
	\item does not allow for integer constraints,
	\item does not allow for crossed input-state terms in the objective function; \ie,   it allows the formulation
	      \begin{align*}
		      \begin{pmatrix}
			      u[k] & x[k]
		      \end{pmatrix} \begin{pmatrix}
			      R & S\top  \\ S & Q
		      \end{pmatrix} \begin{pmatrix}
			      u[k] \\ x[k]
		      \end{pmatrix}
	      \end{align*}
	      uniquely for $S=0$,
	\item does not allow for quadratic constraints, and,
	\item does not allow for semidefinite penalties.
\end{enumerate}

% \textcolor{blue}{Limitations of the solver. Should talk about this somewhere.
%   \begin{enumerate}
%     \item The solver has not been designed with static but with dynamic memory allocation.
%           This prevents the solver from been applied in safety-critical applications, that require the total footprint of the program to be known \textit{a priori}.
%     \item Only one interior-point type solver, whereas other solvers could be desirable in some applications.
%     \item Extensive use of external libraries (BLAS, LAPACK), which goes against the spirit of embedded applications where there are tight memory limitations.
%     \item Supports only \textbf{single} and \textbf{double precision arithmetic}.
%           No support for fixed-point arithmetic, which may be desirable in embedded applications.
%   \end{enumerate}}

% \textbf{ARDA.} Above, I do not agree with you on 1, 3 and 4. You can mention about
% 1 in a positive manner in the conclusion: ``possible extension to the solver would
% be to include static memory allocation.'' 3 is also wrong. You have wrapped BLAS
% and LAPACK calls. If I do not have a proper installation for the corresponding
% platform, I simply need to define them myself. This is true regardless of the
% availability of the custom solver I might find. And, as for 4, you are again wrong.
% Any person who would like to have support for fixed point arithmetic needs to have
% the corresponding package (types with their operations defined) installed. Then,
% you would be supporting them. Obviously, that person need to template specialize
% BLAS and LAPACK calls accordingly.

\section{Thesis outline}
The thesis has the following structure:
\begin{enumerate}
	% \item \textit{Introduction}: Chapter 1 describes the problem, gives the
	%       motivation for this work and sets the goals.
	\item \textit{Background}: Chapter 2 gives the theoretical background on
	      model predictive control and quadratic programming problems. It also
	      provides further requirements of an efficient solver for embedded optimization
	      and presents the major challenges and difficulties of the task.
	\item \textit{State of the Art}: Chapter 3 reviews existing approaches for
	      convex QP in the MPC framework, noting the advantages and disadvantages of each
	      method.
	\item \textit{Proposed Approach}: Chapter 4 presents the proposed optimization method in detail.
	\item \textit{System architecture}: Chapter 5 describes the software
	      architecture. Its modular design is reported, with emphasis on how it helps to
	      attain the goals of efficacy, robustness, flexibility and efficiency.
	\item \textit{Numerical results}: In Chapter 6, we report the performance of the software on
	      numerical experiments.
	\item \textit{Conclusions:} Chapter 7 summarizes the results of the work,
	      critically analyzes the outcome (the final software) and suggests future work.
\end{enumerate}
