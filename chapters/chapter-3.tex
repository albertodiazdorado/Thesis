\chapter{State of the Art}\label{ch3:stateoftheart}


In this chapter, we review the state-of-the-art algorithms for solving quadratic programs in the MPC framework.
\emph{Offline} MPC constitutes a category on its own and we review it first.
\emph{Online} methods, contrarily, is a very generic term that covers many different algorithms, which may be classified according to diverse criteria.
For example, QPs can be solved in the primal or in the dual space (or in both at the same time).
Also, algorithms may be best suited for dense or sparse MPC formulations.
Another important characteristic is whether the MPC formulation has full linear constraints or is limited to box constraints in the form $lb\leq u_k \leq ub$.

In the following, we will classify the algorithms according to the \emph{order} of the information they use.
In that sense, \emph{first order} methods use the information from first order derivatives, whereas \emph{second order} mathods make use of second derivatives.

\section{Explicit MPC}
It is well known that the optimizer $U^*$ for problem \eqref{ch2:MPC} is a picewise affine on polyhedra function of the initial state~\cite{2017-Borrelli}.
The explicit MPC approach has two phases: an offline and an online phase.

In the offline phase, the feasible set is divided into regions and the picewise affine control law is calculated via multi parametric programming as in~\cite[Chapter 12]{2017-Borrelli}.
This step involves the main computational burden, but it can be moved offline.

Then, since the optimal control law is explicitely available, the online phase of the controller simplifies to allocating the initial state to the correct region and reading the control law from a look-up table.

The main advantage of explicit MPC is that the optimization problem is parametrized with respect to $x_0$ and solved offline.
However, this method has limited application in practice, because the number of regions where $x_0$ can be allocated grows exponentially with the number of constraints~\cite{2016-Santin}.
This results in rapidly increasing memory requirements (to store the explicit control laws for each region) and also required time to correctly allocate the initial state in the look-up table, which make this approach infeasible for medium to large size problems.

\section{First Order Methods}
First order methods obtain the search direction in every iteration using gradient information, but need not to compute second derivatives.
This has a few consequences that are common to all first order methods.
On the one hand, every iteration is inexpensive to compute, since the second order information is disregarded.
On the other hand, the search directions at every iterate are worse than in second order methods, which results in a converge rate which is at most superlinear.

Despite slower convergence rates, first order methods have proven to be competitive in practice and there are examples of succesful applications of them to MPC problems.
Finally, it is also worth noting that these methods are very sensitive to the condition number of the Hessian matrix $H$, and because of this, pre-conditioning is usually a common feature of all algorithms.

\subsection{Gradient Projection Method}
The Gradient Projection Method (GPM) exists both in primal and dual versions and is the simplest first order method.
It is a generalization of the steepest descent method for unconstrained optimization: the gradient at every iterate is used as the search direction, which is later projected onto the feasible set if a constraint is hit~\cite{2016-Santin}.
The $k$-th iteration of the GPM is
%
\begin{align}\label{ch3:GPM}
	z^{k+1} & = \func[P_\mathcal{F}]{ z^k-\alpha^k \func[\nabla]{\func[V]{z}} }
\end{align}
%
Where $\func[P_{\mathcal{F}}]{\cdot}$ is the projection operator onto $\mathcal{F}$, ${\func[\nabla]{\func[V]{z}} \coloneqq Hz}$ is the gradient of the cont function at $z$, and $\alpha^k$ is the step size at iterate $k$.

The most expensive operation in (\ref{ch3:GPM}) is the projection operator P$ _\mathcal{F}$.
It can be calculated efficiently for simple constraints, such as sphere or box constraints, but otherwise the projection operator involves solving a QP on its own.
For this reason, primal GPM is limited to MPC problems where the only constraints are box constraints.
Due to this limitation, GPM is not common in practical applications.
Nonetheless, it is always possible to solve the problem in dual space, since the feasible region of the dual variables is the positive orthant and the projection is thus inexpensive to compute.

Another disadvantage of GPM is that it shows a linear convergence rate, since it becomes a version of steepest descent after the optimal basis has been identified~\cite{2016-Santin}.
There have been attempts to overcome this difficulty by adding second-order improvement steps to the pure first-order methods, see~\cite{2008-Axehill}.

\subsection{Fast Gradient Projection Method}
Fast Gradient Projection Methods (FGM) are essentially the same as GPM, and, as such, they share most characteristics (inexpensive iterations, limitation to simple constraints or dual space and high sensitivity to Hessian condition number).
However, FGM achieve a superlinear convergence rate by accumulating momentum: the search direction is not the gradient at the current iterate, but a combination of it and the accumulated gradient at the last iterate.
Intuitively, accumulating gradient information is a means of using second order information without explicitely computing second order derivatives.

An additional advantage of FGM is that it has been object of extensive research in the MPC community.
For example, tight bounds on the maximum number of iterations are available~\cite{2014-Patrinos}, which is advantageous for embedded MPC applications.
Even a fixed-point implementation has been developed that prevents overflow and limits round-off error propagation by adequately choosing the number of integer and fractional bits~\cite{2015-Patrinos}.
Both examples are dual FGM algorithms.


\section{Second Order Methods}
As opposed to first order methods, each iteration of second order method is expensive to compute, but in turn fewer iterations are needed.
There are two families of second order methods: active set methods, whose iterates move along the boundary of the feasible set, and interior point methods, whose iterates lie in the strict interior of the feasible region.
A common advantage to all second order methods is that they are rather insensitive to the Hessian condition number.

\subsection{Active Set Methods}
Active set methods (ASM) are an enhancement of the Simplex method for Linear Programming and, as such, they attempt to identify the optimal active set in a finite number of iterations.
Starting from an arbitrary \emph{working set} of inequality constraints, a sequence of equality constrained QP subproblems is solved.
In every QP, the constraints in the working set are treated as equality constraints and the rest are disregarded.
The result of every subproblem determines whether a constraints should be added to or removed from the working set.
This update procedure continues until the optimal basis for the problem is identified.

Active set methods exist both in primal, dual and primal-dual versions.
Furthermore, they normally favour the dense QP formulation, since they cannot really take advantage of sparsity in the matrices due to the required factorizations and associated fill-in phenomena.

In all versions, ASM show the following common characteristics.
In the first place, there is no tight bound on the maximum number of iterations that ASM require to converge to the minimizer.
The maximum iteration count grows exponentially with the problem size, and examples can be built where the ASM has to try all possible constraint combinations before finding the optimal one.
However, ASM have proven to require a much lower number of iterations for convergence in practice and excel at a feature that other methods cannot effectively benefit from: \emph{warm start}.

We speak of warm start when a good initial guess of the optimal active set is available.
In such case, few iterations will be needed to converge, whereas a bad initial guess can lead to poor performance of the algorithm.
In the MPC framework, ASM is best-suited to steady-state operation, where few changes in the control actions are needed.
Attempts have been made to accelerate the identification of the optimal active set by adding or removing several constraints in every iteration.
One of the most notable examples is qpOases, a multi-parammetric active set method~\cite{2016-Ferreau}.
Other approach is the combination of first-order methods for fast active set identification and active-set methods for fast, quadratic convergence rates~\cite{2016-Santin}.

In the second place, every iteration of the ASM is expensive to compute when compared to first order methods.
This characteristic is common to all second order methods.
As opposed to first order methods, in which the calculation of the search direction involves at most matrix-vector operations, second order methods requires the solution of an indefinite linear system of equations.
This system of linear equations is sometimes referred to as \emph{KKT system} and can be very large depending on the problem dimensions, which makes efficient linear algebra of great importance.
Different methods have been developed for solving the KKT system in the MPC framework, which we review in Chapter \ref{ch4:approach}.

Finally, there are examples of dual ASM as well.
Primal ASM require a feasible initial point which shall be calculated in a so-called \emph{initial phase} of the algorithm.
In contrast, Non Feasible ASM exploits the fact that a feasible dual point is always inmediately available, thus avoiding this inconvinience~\cite{2008-Milman}.

\subsection{Interior Point Methods}
Interior point methods (IPM) receive their name because they do not explore the bundaries of the feasible set, nor do they attempt to identify the optimal active set.
Instead, IPM attempt to directly solve the KKT conditions~\eqref{ch2:stationarity}~-~\eqref{ch2:slackness}, which have an unique solution despite being nonlinear (Theorems \ref{ch2:global}, \ref{ch2:unique_p} and \ref{ch2:unique_d}).

Newton's method is used to solve the system of nonlinear equations.
That is, a sequence of linearized KKT systems is solved, each of them closer to the final solution.
However, a typical problem of IPM is that the KKT conditions become highly nonlinear near the boundary of the feasible set, resulting in a poor, ill-conditioned linearization if the initial iterate is not close to the minimizer.

To overcome this difficulty, the \emph{central path} is introduced.
The central path is the set of suboptimal solutions for perturbed KKT systems, where \eqref{ch2:slackness} is substituted by:
%
\begin{align}\label{ch3:pert_slackness}
	\lambda_i (Ax_i-b_i) & = t_k,\quad i=1,\ldots,m
\end{align}
%
Where $t$ is a strictly positive scalar.
This perturbation can be explained in terms of a logarithmic barrier method, and the interested reader is referred to~\cite[Chapter 16]{2008-Griva}.
Here it is sufficient to know that the minimizer to the optimization problem lies in the interior of the feasible region for any $t > 0$.
The approach of all IPM is to solve a sequence of linearized KKT systems where the parameter $t$ is progressively reduced.
This way, the optimal solution is approached from the interior of the feasible region, avoiding the ill-conditioning that occurs near the boundaries.

IPM do not find the exact solution for the optimization problem, as it lies in the limit $t \rightarrow 0$.
Nonetheless, we can get a solution as accurate as desired by continuously decreasing $t$.
In general, different methods differ in their strategy to perturb the KKT condition and decrease the parameter $t$.
The most succesful IPM are primal-barrier and primal-dual methods.
Each iteration of a primal-dual methods is generally more expensive to compute, since the dual variables are incorporated into the problem.
In turn, such methods are better conditioned than primal-barrier ones.

As in ASMs, not only the algorithm, but also the solution of every linearized KKT system is a concern for IPMs.
This step is more crucial here, because IPMs take all constraints into account at the same time, thus resulting in KKT systems typically larger than those in ASM.
Though every IPM iterate is computationally expensive when compared to ASM, the iteration count is normally low and rather constant, in a range of at most 75 to 100 iterations for large problems.

One of the main advantages of IPM is that they are well suited for exploiting the sparsity and the structure of the matrices in the problem formulation.
We have already seen that the sparse MPC formulation produces very sparse matrices.
In fact, the matrices are either block diagonal ($H, F$) or block bi-diagonal ($C$).
The MPC community has put great emphasis into preserving and exploiting the sparsity patterns in MPC problems, with notorious reduction in the computational complexity as in~\cite{2010-Wang,2012-Domahidi}.
