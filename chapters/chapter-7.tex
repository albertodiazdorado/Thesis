\chapter{Conclusions}

\section{Summary}
This thesis adresses the implementation of an efficient optimization method for embedded MPC applications.

First, we reviewed the theoretical properties of MPC and the arising convex QPs (Chapter~\ref{ch2:background}).
Then, we reviewed the main algorithms present in the literature, and chose one among them taking into account the particularities of our optimization problem (Chapter~\ref{ch3:stateoftheart}).

In our software, we used the algorithm proposed by Mehrotra~\cite{1992-Mehrotra}, which excels in practical applications, and the sparse KKT system scheme proposed in~\cite{2010-Wang,2012-Domahidi} (Chapter~\ref{ch4:approach}).
Furthermore, we exploited some features of \code{C++} to optimize the execution of the code as much as possible (Chapter~\ref{ch5:implementation}).
This allows us to have a software capable of exploiting every nuance in the MPC formulation to speed up the process, yet avoiding code branching and reducing the memory footprint.
These are desirable properties in embedded devices, with limited computational power and memory.

Finally, we reported experimental results of a realistic MPC benchmarking problem.
We compared our implementation against an state-of-the-art active set method, qpOASES~\cite{2014-Ferreau}.
qpOASES is open-source, is widely used in practical applications and is also commonly used for benchmarking purposes.

In our studies, we analyzed different performance measurements.
We focused primarily in worst-case computation time, mean computation time and accuracy of the solution.

For a comprenhensive benchmarking, we simulated the control of different systems, with different set-ups.
We run simulations of systems with varying number of states, inputs and prediction horizons.
We also included random disturbances in our system, since MPC controllers usually have to deal with model inaccuracies and disturbances of any kind.
Finally, we explored how our solver behaves with the inclusion of soft constraints, which is an utmost desirable property in industrial MPC applications.

\section{Results}
Our results are in general in accordance with the theory.
We can observe that qpOASES shows, in general, much lower mean computation times.
We did expect these results, since active set methods have very good warm-start capabilities.
If there is little or no variation at all in the optimal active set, qpOASES can attain the optimum in zero iterations.

Since this is the most usual case in steady-state operation of the controller, we could also expect that qpOASES would show the lowest mean computation time.
In contrast, our solver shows a minimum of three iterations per optimization problem, which seriously impacts the mean computation time.

Nonetheless, the worst-case computation time is similar for both solvers in small problems, and much for our implementation in large problems.
We did also expect this behaviour.
We know that active set methods have exponential complexity in the number of variables and constraints.
If the initial guess is not good, the number of required iterations rapidly increases.
In addition, we also know that interior point methods typically behave better in large optimization problems.

These results are of great importance for real-time applications.
The mean computation time is important in simulations, where we aim to reduce the computation time as much as possible.
In contrast, the worst-case execution time is the real bottleneck for real-time systems.
According to our results, our software is, at least, comparable with qpOASES in terms of performance.
According to our performance tests, we favour qpOASES in small problems, and favour our solution in large problems.

The inclusion of soft constraints has a big impact in the simulation results.
Our software includes constraint softening natively.
In contrast, qpOASES does not have an option to soften constraints, and we have to do it manually.

There are different ways of implementing constraint softening.
It is possible to introduce one slack variable per constraint, which is normally disencouraged due to big amount of computational effort.
A popular alternative is to introduce one slack variable per prediction step, which penalizes the maximum constraint violation in that precise step.
Our software implements constraint softening in the latter way.
For the sake of consistency, we followed the same approach for qpOASES, manually modifying the problem matrices to include the slack variables in the formulation.

With this set-up, we observed that qpOASES becomes about one order of magnitude slower (when compared to the scenario with no soft constraints).
This results in comparable mean computation times.
Also, the worst-case computation time clearly favours our implementation, even in small problems.

Finally and foremost, we found important accuracy problems in qpOASES with the inclusion of soft constraints.
After detecting that our implementation and qpOASES were reporting fairly different solutions to the QPs, we included a third optimization package in our tests.
We have used Gurobi for that purpose, which is a commercial, general purpose solver.
Whereas Gurobi is not competitive in terms of execution time, it is rendered robust and accurate for well-conditioned optimization problems.

For most QPs, we observe that our software and qpOASES provide the same correct result.
However, in some critical QPs (roughly, those with a large change in the optimal basis), qpOASES greatly deviates from the optimal solution.
This is illustrated in Figures~\ref{fig:evolutionpn5}, \ref{fig:evolutionpn12} and \ref{fig:evolutionpm4}.

Again, this result was not completely unexpected.
In their documentation, the authors of the qpOASES package warn that using the MPC option may weaken the robustness of their solver.
Nevertheless, according to our experiments, we favour our software whenever soft constraints have to be included in the design.

\section{Fulfillment of the Project Goals}
In Section 1.2 we stated the goals of the project.
We will review them here, one by one.
%
\begin{enumerate}
	\item \textbf{Effectivity.} Throughout our experimentation, we have proven that our solver is effective (\ie, it correctly solves the proposed problems).
	      In spite of small mismatches, our solver always reported the same solution as Gurobi (within a tiny error margin).
	      Both with and without soft constraints, we never encountered a problem instance that our solver could not solve accurately.
	\item \textbf{Robustness.} We have not completely attained robustness in our software.
	      We do not provide any support against infeasible problems, and we do not handle almost any degeneracy (for example, we require the penalty matrices to be strictly positive definite).

	      However, we provide a consistent constraint softening feature.
	      By taking advantage of such, the user can enforce every single problem to be feasible, which is sufficient for most practical MPC applications.
	\item \textbf{Flexibility.} In our initial design, we were aiming to tackle as many different MPC formulations are possible.
	      This requirement motivates the choice of a modular software design.
	      Whereas we have covered a wide set of formulations (unconstrained and simple bounded variables, both as hard and soft constraints), the software prototype is still incomplete.
	      Due to time limitations, we have not implemented the modules capable of handling linear and full constraints.

	      Another limitation is that we do not support defective bounds; \ie, if the input vector is bounded, all the inputs must be lower and upper bounded.

	      Finally, as we stated before, we do not support positive semidefinite penalty matrices.
	      However, due to our modular design, we hope that this features can be implemented in the future at a low marginal cost.
	\item \textbf{Efficiency.} For the implemented features, we have carried out extensive testing against one state-of-the-art solver, namely qpOASES.
	      Though we report larger mean computation times, we also report a similar speed in worst-case scenarios.
	      qpOASES is certainly superior to our software in small to medium, hard constrained problems.
	      Nevertheless, we have also found use cases were we report better solution times than qpOASES, with potential for practical use of our software.
\end{enumerate}
%
Even though not all the goals have been fully attained, we hope that we can reach them in the future, as explained in the next section.

\section{Future Work}
Regarding the future work, we would like to make a distinciton between the items related to the algorithm, and the items related to the software itself.
The first refers to a theoretical (and possibly empirical/experimental) investigation, whereas the second refers to the implementation of the code.

Regarding the algorithm that we use, future research may include:
%
\begin{enumerate}
	\item \textbf{Improved warm start \- }We have seen that, unlike active set methods, our approach requires a minimum number of iterations for every QP.
	      This holds even if two consecutive QPs have very close solutions (\ie, the solutions belong to the same active set).
	      Warm-start of interior point methods is still an open question in the research community.
	      Any strategy (or heuristics) capable of advantageously warm starting the algorithm would have a very significative impact in the mean computation time of the solver.
	\item \textbf{Inconsistent linesearch strategies \- }In linear optimization, interior point methods typically use different step sizes for primal and dual variables.
	      Here, we are constrained to using the same step size for both sets of variables.
	      The reason for this is that the primal and dual variables are coupled via the Hessian matrix in the stationarity condition~\ref{ch2:stationarity}.
	      Using different step sizes would result in not fulfilling the first block equation in~\ref{ch4:KKT} in the next iterate.
	      However, using different step sizes also means taking longer steps, which may overcome the drawbacks of producing a larger residual $r_c$ (under certain circumstances).
	\item \textbf{Stepsize scaling parameter \- }As discussed in Chapter~\ref{ch4:approach}, regarding Algorithm~\ref{alg:Mehrotra}, the final step size is not applied entirely but scaled by a constant $\eta < 1$.
	      Researchers~\cite{2004-Boyd,1997-Wright,2000-Nocedal} have found this scaling useful in practice.
	      Roughly, the parameter $\eta$ prevents the pairs $\lambda$ - $s$ from becoming too unbalanced, which may lead to numerical instabilities in the KKT matrix.

	      However, there is no general agreement regarding the best choice of $\eta$, which relies on heuristics, and varies between values as different as $0.9$ and $0.9999$ in real applications.
	      Moreover, to our knowledge there is no theoretical analysis about the influence of this parameter.
	      Hence, we consider of interest studying the influence of the scaling factor $\eta$ in the overall performance of the algorithm, either theoretically or empirically.
\end{enumerate}
%

Regarding the final software, future development may consist on:
%
\begin{enumerate}
	\item \textbf{Static memory allocation support \- }As explained in Section 5.8, static memory allocation poses important advantages in certain applications.
	      Because the aim of the project is developing a solver that can run in embedded devices, static memory allocation is a main concern, and a future development of the software should take this into account.
	\item \textbf{Defective bounds \- }The solver, as we conceived it, only allows for \emph{complete} bounds.
	      That means that, if we decide that the system state is bounded, then all the states must be upper and lower bounded.
	      Though apparently restrictive, this is a rather typical scenario if we consider real systems, in which signals are rarely allowed to grow towards infinity or minus infinity.

	      Nonetheless, even if that is the case, the user may easily circumvent this problem by providing a large bound, that we know that the system will never reach.
	      That makes the variable, in practice, behave as an unbounded variable.
	      Nevertheless, we can think, design and develop a new policy that handles defective bounds explicitely, which would easen the design task of the user.
	\item \textbf{Crossed input-state penalties \- }In our design, we are restricted to penalties in the form
	      %
	      \begin{align*}
		      u_k^\top R u_k + x_k^\top Q x_k
	      \end{align*}
	      %
	      for positive definite penalty matrices $R$ and $Q$.
	      This is a rather general formulation, and most linear MPC formulations fall into this category.
	      However, it is also possible to penalize the crossed products input-state, as in:
	      %
	      \begin{align*}
		      u_k^\top R u_k + x_k^\top Q x_k + 2u_k^\top S x_k.
	      \end{align*}
	      %
	      The inclusion of the crossed penalty term $S$ in the sparse KKT matrix factorization has already been explored in~\cite{2012-Domahidi}.
	      Adapting our software to using crossed penalties would enable the user to even more design freedom.
	\item \textbf{Nonlinear MPC wrapper \- }A popular nonlinear optimization strategy is to solve a second order approximation of the optimization problem at each iterate, which provides the new search direction.
	      That is the case of sequential quadratic programming, but also of some interior point methods (see LANCELOT,~\cite{2010-Conn}).
	      Hence, there a few nonlinear optimization algorithms that rely on solving a sequence of QPs with varying matrices.
	      Our algorithm adapts relatively well to varying problem matrices, since we do not reuse matrix factorizations from one iteration to another (as ASMs do).
	      For that reason, designing a nonlinear MPC solver that wraps around our current optimization engine is a possible future development direction.
\end{enumerate}
%
